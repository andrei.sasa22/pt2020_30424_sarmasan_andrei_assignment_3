package ro.utcn.enums;

public enum Operation {

    INSERT,
    DELETE,
    ORDER,
    REPORT

}
