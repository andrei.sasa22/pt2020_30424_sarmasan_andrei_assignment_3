package ro.utcn.businessLayer;

import ro.utcn.model.Product;
import ro.utcn.dataAccessLayer.OrderDAO;
import ro.utcn.dataAccessLayer.ProductDAO;
import ro.utcn.presentation.PDF;

public class ProductService {

    private ProductDAO productDAO = ProductDAO.getProductDAO();

    private OrderDAO orderDAO = OrderDAO.getOrderDAO();

    /**
     * models the object and calls for the repository to add it to the database
     * @param name name of the product
     * @param quantity quantity of the product
     * @param price price per unit
     */
    public void add(final String name, final Integer quantity, final Double price){
        if(productDAO.findByName("name", name) == null) {
            final Product product = new Product();
            product.setId(productDAO.nextId());
            product.setName(name);
            product.setPrice(price);
            product.setQuantity(quantity);
            productDAO.insert(product);
        } else {
            final Product productFound = (Product) productDAO.findByName("name", name).get(0);
            productDAO.updateIntegerField(productFound.getId(), "quantity", productFound.getQuantity() + quantity);
        }
    }

    /**
     * calls for repository to delete the orders and clients containing the specified name
     * @param name name used for the identification of the object
     */
    public void delete(final String name) {
        final Product productFound = (Product) productDAO.findByName("name", name).get(0);
        if(productFound != null) {
            orderDAO.deleteByForeignKey("productId", productFound.getId());
            productDAO.delete(name);
        } else {
            System.out.println("ERROR: Product " + name + " does not exist.");
        }
    }

    /**
     * calls for repository to get all the clients that are going to be used by the PDF in order to create a report
     */
    public void report() {
        PDF.getPdf().productReport(productDAO.getAll());
    }

    private static ProductService productService;

    private ProductService() {
    }

    /**
     * @return singleton instance of the class
     */
    public static ProductService getProductService() {
        if(productService == null) {
            productService = new ProductService();
        }
        return productService;
    }
}
