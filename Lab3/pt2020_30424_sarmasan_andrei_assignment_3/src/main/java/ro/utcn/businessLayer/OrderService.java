package ro.utcn.businessLayer;

import ro.utcn.model.Client;
import ro.utcn.model.Orders;
import ro.utcn.model.Product;
import ro.utcn.dataAccessLayer.ClientDAO;
import ro.utcn.dataAccessLayer.OrderDAO;
import ro.utcn.dataAccessLayer.ProductDAO;
import ro.utcn.presentation.PDF;

public class OrderService {

    private OrderDAO orderDAO = OrderDAO.getOrderDAO();

    private ClientDAO clientDAO = ClientDAO.getClientDAO();

    private ProductDAO productDAO = ProductDAO.getProductDAO();

    /**
     * models the object and calls for the repository to add it to the database
     * @param clientName name of the client
     * @param product name of the product
     * @param quantity quantity of the product
     */
    public void add(final String clientName, final String product, final Integer quantity) {
        final Client clientFound = (Client) clientDAO.findByName("name", clientName).get(0);
        if(clientFound != null) {
            final Product productFound = (Product) productDAO.findByName("name", product).get(0);
            if(productFound != null) {
                if(productFound.getQuantity() >= quantity) {
                    Orders order = new Orders();
                    order.setId(orderDAO.nextId());
                    order.setClientId(clientFound.getId());
                    order.setProductId(productFound.getId());
                    order.setQuantity(quantity);
                    orderDAO.insert(order);
                    productDAO.updateIntegerField(productFound.getId(), "quantity", productFound.getQuantity() - quantity);
                    PDF.getPdf().createBill(order.getId(), clientName, product, quantity, productFound.getPrice());
                } else {
                    PDF.getPdf().createUnderstockedBill(product, productFound.getQuantity(), clientName, quantity);
                }
            } else {
                System.out.println("ERROR: Product " + product + " not found.");
            }
        } else {
            System.out.println("ERROR: Client " + clientName + " not found.");
        }
    }

    /**
     * calls for repository to get all the clients that are going to be used by the PDF in order to create a report
     */
    public void report() {
        PDF.getPdf().orderReport(orderDAO.getAll());
    }

    private static OrderService orderService;

    private OrderService() {
    }

    /**
     * @return singleton instance of the class
     */
    public static OrderService getOrderService() {
        if(orderService == null) {
            orderService = new OrderService();
        }
        return orderService;
    }
}
