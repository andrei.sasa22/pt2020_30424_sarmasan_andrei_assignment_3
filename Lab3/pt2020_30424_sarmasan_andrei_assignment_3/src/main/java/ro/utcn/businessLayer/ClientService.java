package ro.utcn.businessLayer;

import ro.utcn.model.Address;
import ro.utcn.model.Client;
import ro.utcn.dataAccessLayer.AddressDAO;
import ro.utcn.dataAccessLayer.ClientDAO;
import ro.utcn.dataAccessLayer.OrderDAO;
import ro.utcn.presentation.PDF;

public class ClientService {

    private ClientDAO clientDAO = ClientDAO.getClientDAO();

    private AddressDAO addressDAO = AddressDAO.getAddressDAO();

    private OrderDAO orderDAO = OrderDAO.getOrderDAO();

    /**
     * models the object and calls for the repository to add it to the database
     * @param name name of the client on which the operation is performed
     * @param addressName name of the address of the client on which the operation is performedd
     */
    public void add(final String name, final String addressName) {
        if(clientDAO.findByName("name", name) == null) {
            final Address address = new Address();
            address.setId(addressDAO.nextId());
            address.setName(addressName);
            addressDAO.insert(address);
            final Client client = new Client();
            client.setId(clientDAO.nextId());
            client.setName(name);
            client.setAddressId(address.getId());
            clientDAO.insert(client);
        } else {
            System.out.println("ERROR: Client " + name + " already exists.");
        }
    }

    /**
     * calls for repository to delete the orders and clients containing the specified name
     * @param name name of the client on which the operation is performed
     */
    public void delete(final String name) {
        if(clientDAO.findByName("name", name) != null) {
            final Client client = (Client) clientDAO.findByName("name", name).get(0);
            orderDAO.deleteByForeignKey("clientId", client.getId());
            clientDAO.delete(name);
        } else {
            System.out.println("ERROR: Client " + name + " does not exist.");
        }
    }

    /**
     * calls for repository to get all the clients that are going to be used by the PDF in order to create a report
     */
    public void report() {
        PDF.getPdf().clientReport(clientDAO.getAll());
    }

    private static ClientService clientService;

    /**
     * @return singleton instance of the class
     */
    public static ClientService getClientService() {
        if(clientService == null) {
            clientService = new ClientService();
        }
        return clientService;
    }
}
