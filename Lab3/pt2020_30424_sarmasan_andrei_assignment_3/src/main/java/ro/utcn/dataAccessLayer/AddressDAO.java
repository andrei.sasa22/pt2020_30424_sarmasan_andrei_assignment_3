package ro.utcn.dataAccessLayer;

import ro.utcn.model.Address;
import ro.utcn.dataAccessLayer.base.AbstractDAO;

public class AddressDAO extends AbstractDAO {

    private static AddressDAO addressDAO;

    private AddressDAO() {
        super(Address.class);
    }

    /**
     * @return singleton instance of the class
     */
    public static AddressDAO getAddressDAO() {
        if(addressDAO == null) {
            addressDAO = new AddressDAO();
        }
        return addressDAO;
    }
}
