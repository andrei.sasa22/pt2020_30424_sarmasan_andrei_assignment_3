package ro.utcn.dataAccessLayer.base;

import ro.utcn.connection.ConnectionFactory;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public abstract class AbstractDAO<MODEL> {

    protected final Class<MODEL> type;

    private final Connection connection = ConnectionFactory.getConnection();

    public AbstractDAO(final Class<MODEL> type) {
        this.type = type;
    }

    /**
     * inserts generic model into database
     * @param element generic model
     */
    public void insert(final MODEL element) {
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement(insertQuery(element.getClass().getDeclaredFields().length));
            int index = 1;
            for(Field field : element.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                try{
                    preparedStatement.setObject(index++, field.get(element));
                } catch(IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            preparedStatement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param numberOfElements number of field that need to be converted into query ? elements
     * @return query string
     */
    private String insertQuery(int numberOfElements) {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO ");
        stringBuilder.append(type.getSimpleName());
        stringBuilder.append(" VALUES (?");
        while (numberOfElements-- > 1) {
            stringBuilder.append(", ?");
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    /**
     * deletes an entry from the database by its name
     * @param name unique identifier used for the delete query
     */
    public void delete(final String name) {
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM " + type.getSimpleName() + " WHERE name = ?");
            preparedStatement.setString(1, name);
            preparedStatement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param field field used for the search
     * @param value value used for the search
     * @return result from query
     */
    public ArrayList<MODEL> findByName(final String field, final String value) {
        return findByFieldAndValue(field, " '" + value + "' ");
    }

    /**
     * @param field field field used for the search
     * @param value value value used for the search
     * @return result from query
     */
    private ArrayList<MODEL> findByFieldAndValue(final String field, final String value) {
        ArrayList<MODEL> list = null;
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + type.getSimpleName() + " WHERE `" + field + "` = " + value);
            final ResultSet resultSet = preparedStatement.executeQuery();
            list = createList(resultSet);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        if((list != null) && (list.isEmpty())) {
            return null;
        }
        return list;
    }

    /**
     * @param resultSet result set from query
     * @return list of generic object converted from the result set
     */
    private ArrayList<MODEL> createList(final ResultSet resultSet) {
        ArrayList<MODEL> list = new ArrayList<MODEL>();
        try {
            while(resultSet.next()) {
                final MODEL newInstance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    final Object object = resultSet.getObject(field.getName());
                    final PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    final Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(newInstance, object);
                }
                list.add(newInstance);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * @param id id of entity that is updated
     * @param field field that needs to be update
     * @param value integer value of the field that is updated
     */
    public void updateIntegerField(final Integer id, final String field, final Integer value) {
        updateFieldByFieldNameAndValue(id, field, value);
    }

    /**
     * @param id id of entity that is updated
     * @param field field that needs to be update
     * @param value integer value of the field that is updated
     */
    private void updateFieldByFieldNameAndValue(final Integer id, final String field, final Integer value) {
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement("UPDATE " + type.getSimpleName() + " SET `" + field +"` = ? WHERE id = ?");
            preparedStatement.setInt(2, id);
            preparedStatement.setInt(1, value);
            preparedStatement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return get all entities of the generic type from the database
     */
    public ArrayList<MODEL> getAll() {
        ArrayList<MODEL> list = null;
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + type.getSimpleName());
            final ResultSet resultSet = preparedStatement.executeQuery();
            list = createList(resultSet);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        if((list != null) && (list.isEmpty())) {
            return null;
        }
        return list;
    }

    /**
     * @return next available id from the database, based on auto increment strategy
     */
    public int nextId() {
        int id = -1;
        try {
            final PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + type.getSimpleName() + " ORDER BY id DESC LIMIT 1");
            final ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                id = resultSet.getInt(1) + 1;
            } else {
                id = 1;
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    protected Connection getConnection() {
        return connection;
    }
}
