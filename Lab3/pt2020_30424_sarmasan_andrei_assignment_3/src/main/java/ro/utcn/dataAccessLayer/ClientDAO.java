package ro.utcn.dataAccessLayer;

import ro.utcn.model.Client;
import ro.utcn.dataAccessLayer.base.AbstractDAO;

public class ClientDAO extends AbstractDAO {

    private static ClientDAO clientDAO;

    private ClientDAO() {
        super(Client.class);
    }

    /**
     * @return singleton instance of the class
     */
    public static ClientDAO getClientDAO() {
        if(clientDAO == null) {
            clientDAO = new ClientDAO();
        }
        return clientDAO;
    }
}
