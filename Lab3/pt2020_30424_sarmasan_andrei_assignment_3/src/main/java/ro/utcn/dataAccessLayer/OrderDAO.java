package ro.utcn.dataAccessLayer;

import ro.utcn.model.Orders;
import ro.utcn.dataAccessLayer.base.AbstractDAO;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class OrderDAO extends AbstractDAO {

    private static OrderDAO orderDAO;

    /**
     * @param field name of the foreign key field
     * @param id id of the foreign key
     */
    public void deleteByForeignKey(final String field, final Integer id) {
        final String query = "DELETE FROM " + type.getSimpleName() + " WHERE " + field + " = ?";
        try {
            final PreparedStatement statement = super.getConnection().prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    private OrderDAO() {
        super(Orders.class);
    }

    /**
     * @return singleton instance of the class
     */
    public static OrderDAO getOrderDAO() {
        if(orderDAO == null) {
            orderDAO = new OrderDAO();
        }
        return orderDAO;
    }
}
