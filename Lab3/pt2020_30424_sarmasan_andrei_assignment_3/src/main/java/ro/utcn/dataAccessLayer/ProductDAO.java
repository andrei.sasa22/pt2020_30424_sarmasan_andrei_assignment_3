package ro.utcn.dataAccessLayer;

import ro.utcn.model.Product;
import ro.utcn.dataAccessLayer.base.AbstractDAO;

public class ProductDAO extends AbstractDAO {

    private static ProductDAO productDAO;

    private ProductDAO() {
        super(Product.class);
    }

    /**
     * @return singleton instance of the class
     */
    public static ProductDAO getProductDAO() {
        if(productDAO == null) {
            productDAO = new ProductDAO();
        }
        return productDAO;
    }
}
