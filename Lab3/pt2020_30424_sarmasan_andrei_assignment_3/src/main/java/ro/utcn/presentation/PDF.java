package ro.utcn.presentation;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import ro.utcn.model.Client;
import ro.utcn.model.Orders;
import ro.utcn.model.Product;

import java.io.FileOutputStream;
import java.util.ArrayList;

public class PDF {

    private static PDF pdf;

    private Font headerFont = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD);
    private Font subHeaderFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);

    private int clientNr = 1;
    private int productNr = 1;
    private int orderNr = 1;
    private int understockedNr = 1;

    /**
     * @param id order id
     * @param clientName client name
     * @param productName product name
     * @param quantity quantity of product
     * @param price price per unit
     */
    public void createBill(final Integer id, final String clientName, final String productName, final Integer quantity, final Double price) {
        final String FILE = "billOrderNr" + id + ".pdf";
        try {
            final Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            billHeader(document, id);
            billContent(document, id, clientName, productName, quantity, price);
            document.close();
        } catch (Exception e) {
            System.out.println("ERROR creating " + FILE);
        }
    }

    /**
     * adds the header of the document
     * @param document document the data is added on
     * @param id id of order
     */
    private void billHeader(final Document document, final Integer id) {
        document.addTitle("Order number " + id);
        document.addSubject("Bill");
        document.addAuthor("Order Management App");
        document.addCreator("Order Management App");
    }

    /**
     * @param document document the data is added on
     * @param id order id
     * @param clientName client name
     * @param productName product name
     * @param quantity quantity of product
     * @param price price per unit
     * @throws DocumentException
     */
    private void billContent(final Document document, final Integer id, final String clientName, final String productName, final Integer quantity, final Double price)
            throws DocumentException {
        final Paragraph paragraph = new Paragraph();
        paragraph.add(new Paragraph());
        paragraph.add(new Paragraph("Bill", headerFont));
        paragraph.add(new Paragraph("Order number " + id, normalFont));
        paragraph.add(new Paragraph());
        paragraph.add(new Paragraph("Client name: " + clientName));
        paragraph.add(new Paragraph("Product: " + productName));
        paragraph.add(new Paragraph("Price per unit: " + price));
        paragraph.add(new Paragraph("Quantity: " + quantity));
        paragraph.add(new Paragraph());
        paragraph.add(new Paragraph("TOTAL: " + (quantity * price), subHeaderFont));
        document.add(paragraph);
    }

    /**
     * @param productName name of product
     * @param productQuantity quantity of product
     * @param clientName name of client
     * @param requestedQuantity quantity requested for the understocked bill
     */
    public void createUnderstockedBill(final String productName, final Integer productQuantity, final String clientName, final Integer requestedQuantity) {
        final String FILE = "billUnderstocked" + productName + (understockedNr++) + ".pdf";
        try {
            final Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            understockedHeader(document, productName);
            understockedContent(document, productName, productQuantity, clientName, requestedQuantity);
            document.close();
        } catch (Exception e) {
            System.out.println("ERROR creating " + FILE);
        }
    }

    /**
     * adds the header of the document
     * @param document document the data is added on
     * @param name name of the understocked product
     */
    private void understockedHeader(final Document document, final String name) {
        document.addTitle("Understocked ");
        document.addSubject(name);
        document.addAuthor("Order Management App");
        document.addCreator("Order Management App");
    }


    /**
     * @param document document the data is added on
     * @param productName name of product
     * @param productQuantity quantity of product
     * @param clientName name of client
     * @param requestedQuantity quantity requested
     * @throws DocumentException
     */
    private void understockedContent(final Document document, final String productName, final Integer productQuantity, final String clientName, final Integer requestedQuantity)
            throws DocumentException {
        final Paragraph paragraph = new Paragraph();
        paragraph.add(new Paragraph());
        paragraph.add(new Paragraph("Understocked ", headerFont));
        paragraph.add(new Paragraph(productName, subHeaderFont));
        paragraph.add(new Paragraph());
        paragraph.add(new Paragraph("Client " + clientName + " ordered " + requestedQuantity
                + " units. However, only " + productQuantity + " were available.", normalFont));
        document.add(paragraph);
    }

    private void reportHeader(final Document document, final String name) throws DocumentException {
        document.addTitle("Report");
        document.addSubject(name);
        document.addAuthor("Order Management App");
        document.addCreator("Order Management App");
        final Paragraph paragraph = new Paragraph();
        paragraph.add(new Paragraph());
        paragraph.add(new Paragraph("Report", headerFont));
        paragraph.add(new Paragraph(name, subHeaderFont));
        paragraph.add(new Paragraph());
        document.add(paragraph);
    }

    public void clientReport(final ArrayList<Client> clients) {
        final String FILE = "reportClientNr" + (clientNr++) + ".pdf";
        try {
            final Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            reportHeader(document, "Clients");
            clientContent(document, clients);
            document.close();
        } catch (Exception e) {
            System.out.println("ERROR creating " + FILE);
        }
    }

    private void headers(final PdfPTable table, final String[] fields) {
        for(String field : fields) {
            final PdfPCell c = new PdfPCell(new Phrase(field));
            c.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c);
        }
        table.setHeaderRows(1);
    }

    private void clientContent(final Document document, final ArrayList<Client> clients) throws DocumentException {
        final Paragraph paragraph = new Paragraph();
        final PdfPTable table = new PdfPTable(3);
        final String[] fields = {"ID", "Name", "AddressId"};
        headers(table, fields);
        for(Client client : clients) {
            final PdfPCell pdfPCell = new PdfPCell(new Phrase(String.valueOf(client.getId())));
            table.addCell(pdfPCell);
            table.addCell(client.getName());
            table.addCell(String.valueOf(client.getAddressId()));
        }
        paragraph.add(table);
        document.add(paragraph);
    }

    public void productReport(final ArrayList<Product> products) {
        final String FILE = "reportProductNr" + (productNr++) + ".pdf";
        try {
            final Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            reportHeader(document, "Products");
            addProductsReportContent(document, products);
            document.close();
        } catch (Exception e) {
            System.out.println("ERROR creating " + FILE);
        }
    }

    private void addProductsReportContent(final Document document, final ArrayList<Product> products) throws DocumentException {
        final Paragraph paragraph = new Paragraph();
        final PdfPTable table = new PdfPTable(4);
        final String[] fields = {"ID", "Name", "Quantity", "Price"};
        headers(table, fields);
        for(Product product : products) {
            final PdfPCell pdfPCell = new PdfPCell(new Phrase(String.valueOf(product.getId())));
            table.addCell(pdfPCell);
            table.addCell(product.getName());
            table.addCell(String.valueOf(product.getQuantity()));
            table.addCell(String.valueOf(product.getPrice()));
        }
        paragraph.add(table);
        document.add(paragraph);
    }

    public void orderReport(final ArrayList<Orders> orders) {
        final String FILE = "reportOrderNr" + (orderNr++) + ".pdf";
        try {
            final Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            reportHeader(document, "Orders");
            addOrdersReportContent(document, orders);
            document.close();
        } catch (Exception e) {
            System.out.println("ERROR creating " + FILE);
        }
    }

    private void addOrdersReportContent(final Document document, final ArrayList<Orders> orders) throws DocumentException {
        final Paragraph paragraph = new Paragraph();
        final PdfPTable table = new PdfPTable(4);
        final String[] fields = {"ID", "Client", "Product", "Quantity"};
        headers(table, fields);
        for(Orders order : orders) {
            final PdfPCell pdfPCell = new PdfPCell(new Phrase(String.valueOf(order.getId())));
            table.addCell(pdfPCell);
            table.addCell(String.valueOf(order.getClientId()));
            table.addCell(String.valueOf(order.getProductId()));
            table.addCell(String.valueOf(order.getQuantity()));
        }
        paragraph.add(table);
        document.add(paragraph);
    }

    private PDF() {
    }

    public static PDF getPdf() {
        if(pdf == null) {
            pdf = new PDF();
        }
        return pdf;
    }
}
