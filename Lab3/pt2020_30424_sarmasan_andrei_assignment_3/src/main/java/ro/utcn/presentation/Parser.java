package ro.utcn.presentation;

import ro.utcn.businessLayer.ClientService;
import ro.utcn.businessLayer.OrderService;
import ro.utcn.businessLayer.ProductService;
import ro.utcn.enums.Operation;
import ro.utcn.enums.OperationType;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Parser {

    private Scanner scanner;

    private ClientService clientService = ClientService.getClientService();

    private OrderService orderService = OrderService.getOrderService();

    private ProductService productService = ProductService.getProductService();

    /**
     * @param string path of the file that is connected to the scanner
     */
    public Parser(final String string) {
        try {
            this.scanner = new Scanner(new File(string));
        } catch (FileNotFoundException e) {
            System.out.println("File " + string + " does not exist!");
        }
    }

    /**
     * parses through each line and determines the type of the operation that needs to be performed
     */
    public void parse() {
        try {
            while (scanner.hasNext()) {
                final String string = scanner.nextLine();
                final String[] elements = string.split(": ");
                final String[] first = elements[0].split(" ");
                final Operation operation = getOperation(string, first);
                final OperationType operationType = getOperationType(first, operation);
                final String parameters = elements.length == 2 ? elements[1] : null;
                operate(operation, operationType, parameters);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param string command
     * @param first first keyword from the command
     * @return the operation that needs to be performed
     */
    private Operation getOperation(final String string, final String[] first) {
        if ("insert".equals(first[0].toLowerCase())) {
            return Operation.INSERT;
        } else if ("delete".equals(first[0].toLowerCase())) {
            return Operation.DELETE;
        } else if ("order".equals(first[0].toLowerCase())) {
            return Operation.ORDER;
        } else if ("report".equals(first[0].toLowerCase())) {
            return Operation.REPORT;
        }
        System.out.println("Invalid command " + string + ".");
        return null;
    }

    /**
     * @param first first keyword from the command
     * @param operation operation that needs to be performed
     * @return the type of operation that needs to be performed
     */
    private OperationType getOperationType(final String[] first, final Operation operation) {
        if ((first.length == 2) && ((operation == Operation.INSERT) || (operation == Operation.DELETE) || operation == Operation.REPORT)) {
            if ("client".equals(first[1].toLowerCase())) {
                return OperationType.CLIENT;
            } else if ("product".equals(first[1].toLowerCase())) {
                return OperationType.PRODUCT;
            } else if ("order".equals(first[1].toLowerCase())) {
                return OperationType.ORDER;
            }
        }
        return null;
    }

    /**
     * @param operation operation that needs to be performed
     * @param operationType type of operation that needs to be performed
     * @param parameters extra parameters
     */
    private void operate(final Operation operation, final OperationType operationType, final String parameters) {
        if(operation != null) {
            switch (operation) {
                case INSERT:
                    insert(parameters, operationType);
                    break;
                case DELETE:
                    delete(parameters.split(",")[0], operationType);
                    break;
                case ORDER:
                    order(parameters);
                    break;
                case REPORT:
                    report(operationType);
                    break;
            }
        }
    }

    private void insert(final String parameters, final OperationType operationType) {
        if (operationType == OperationType.CLIENT) {
            final String[] params = parameters.split(", ");
            if (params.length == 2) {
                clientService.add(params[0], params[1]);
            }
        } else if (operationType == OperationType.PRODUCT) {
            final String[] params = parameters.split(", ");
            if (params.length == 3) {
                int quantity = Integer.parseInt(params[1]);
                double price = Double.parseDouble(params[2]);
                productService.add(params[0], quantity, price);
            }
        }
    }

    private void delete(final String parameters, final OperationType operationType) {
        if (operationType == OperationType.CLIENT) {
            clientService.delete(parameters);
        } else if (operationType == OperationType.PRODUCT) {
            productService.delete(parameters);
        }
    }


    private void order(final String parameters) {
        final String[] params = parameters.split(", ");
        if (params.length == 3) {
            int quantity = Integer.parseInt(params[2]);
            orderService.add(params[0], params[1], quantity);
        }
    }

    private void report(final OperationType operationType) {
        switch (operationType) {
            case CLIENT:
                clientService.report();
                break;
            case PRODUCT:
                productService.report();
                break;
            case ORDER:
                orderService.report();
                break;
        }
    }

    public void close() {
        scanner.close();
    }

}
