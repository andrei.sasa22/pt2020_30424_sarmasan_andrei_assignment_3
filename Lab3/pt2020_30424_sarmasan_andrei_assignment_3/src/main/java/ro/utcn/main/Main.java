package ro.utcn.main;

import ro.utcn.presentation.Parser;

public class Main {

    public static void main(String[] args) {
        final Parser parser = new Parser(args[0]);
        parser.parse();
        parser.close();

    }
}
