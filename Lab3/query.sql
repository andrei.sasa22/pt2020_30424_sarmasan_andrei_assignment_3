CREATE DATABASE order_management;
USE order_management;

DROP TABLE IF EXISTS address;
CREATE TABLE address (
id INT PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR(45) NOT NULL DEFAULT ''
) ENGINE = InnoDB;

DROP TABLE IF EXISTS client;
CREATE TABLE client (
id INT PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR(45) NOT NULL DEFAULT '',
addressid INT,
FOREIGN KEY(addressid) REFERENCES address (id)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS product;
CREATE TABLE product (
id INT PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR(45) NOT NULL DEFAULT '',
quantity INT,
price DOUBLE
) ENGINE = InnoDB;

DROP TABLE IF EXISTS orders;
CREATE TABLE orders (
id INT PRIMARY KEY AUTO_INCREMENT,
clientid INT,
productid INT,
quantity INT,
FOREIGN KEY(clientid) REFERENCES client (id),
FOREIGN KEY(productid) REFERENCES product (id)
) ENGINE = InnoDB;

SELECT * FROM address;
SELECT * FROM client;
SELECT * FROM product;
SELECT * FROM orders;